﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="packageinstaller.ascx.cs" Inherits="USN_Inspired_Package.usn_inspired_package.packageinstaller" %>
<asp:Literal ID="litMessage" runat="server"></asp:Literal>
<asp:Panel ID="pnlSuccess" runat="server" Visible="false">
    <h3>uSkinned Inspired Starter Kit Successfully Installed</h3>
    <br/>
    <p>The installer has setup some pages to get you started.</p>
    <p>To understand all of the layouts and components available please refer to the online demo available on the uSkinned website.</p>
    <p><a href="http://uskinned.net/demos/1419/" target="_blank" title="Link will open in a new window/tab">Inspired Demo</a></p>
    <p>Go to the "Content" section to manage the installed website.</p>
</asp:Panel>