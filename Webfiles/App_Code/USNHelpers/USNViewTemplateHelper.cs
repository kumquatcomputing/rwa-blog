﻿using System;
using Umbraco.Core.Models;
using USNStarterKit.USNEntities;
//using USNStarterKit.USNBackgroundOptions.Models;

namespace USNStarterKit.USNHelpers
{
    public static class USNViewTemplateHelper
    {
        public static USNTemplateStyleSettings GetTemplateStyleSettings(string backgroundColor = null, string buttonColor = null, string panelColor = null)
        {
            USNTemplateStyleSettings settings = new USNTemplateStyleSettings();
            //Hex values are case-sensitive and must match those found in the 'USN Color' datatype.


            if (!String.IsNullOrEmpty(backgroundColor))
            {
                switch (backgroundColor)
                {

                    case "004775": /*dark blue*/
                        settings.BackGroundStyle = "c1-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        break;
                    case "666666": /*grey*/
                        settings.BackGroundStyle = "c2-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        //settings.TextStyle = String.Empty;
                        break;
                    case "1d1d1b": /*black*/
                        settings.BackGroundStyle = "c3-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        break;
                    case "00cc99": /*light green*/
                        settings.BackGroundStyle = "c4-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        break;
                    case "ffffff": /*white*/
                        settings.BackGroundStyle = "c5-bg";
                        settings.HeadingStyle = "c3-text";
                        settings.TextStyle = "c3-text";
                        break;
                    case "482c78": /*purple*/
                        settings.BackGroundStyle = "c6-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        break;
                    case "f89721": /*orange*/
                        settings.BackGroundStyle = "c7-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        break;
                    case "00aeef": /*light blue*/
                        settings.BackGroundStyle = "c8-bg";
                        settings.HeadingStyle = "c5-text";
                        settings.TextStyle = "c5-text";
                        break;

                    default:
                        settings.BackGroundStyle = "c5-bg";
                        settings.HeadingStyle = "c3-text";
                        //settings.TextStyle = String.Empty;
                        settings.TextStyle = "c3-text";
                        break;
                }
            }
            else
            {
                settings.BackGroundStyle = "c5-bg";
                settings.HeadingStyle = "c3-text";
                //settings.TextStyle = String.Empty;
                settings.TextStyle = "c3-text";
            }


            //if (Model.Content.HasValue("buttonColor"))
            //{
            //    switch (Model.Content.GetProperty("buttonColor").Value.ToString())
            //    {
            //        case "a05cc0":
            //            buttonStyle = "c1-borders c1-text";
            //            break;
            //        case "cccccc":
            //            buttonStyle = "c2-borders c2-text";
            //            break;
            //        case "151515":
            //            buttonStyle = "c3-borders c3-text";
            //            break;
            //        case "f4f4f4":
            //            buttonStyle = "c4-borders c4-text";
            //            break;
            //        case "ffffff":
            //            buttonStyle = "c5-borders c5-text";
            //            break;
            //        case "eeeeee":
            //            buttonStyle = "c6-borders c6-text";
            //            break;
            //        default:
            //            buttonStyle = "c1-borders c1-text";
            //            break;
            //    }
            //}


            if (!String.IsNullOrEmpty(buttonColor))
            {
                switch (buttonColor)
                {
                    case "004775":/*dark blue*/
                        settings.ButtonStyle = "c1-btn c5-borders c5-text c1-btn";
                        break;
                    case "666666":/*grey*/
                        settings.ButtonStyle = "c2-btn c5-borders c5-text c2-btn";
                        break;
                    case "1d1d1b":/*black*/
                        settings.ButtonStyle = "c3-btn c5-borders c5-text c3-btn";
                        break;
                    case "00cc99":/*light green*/
                        settings.ButtonStyle = "c4-btn c5-borders c5-text c4-btn";
                        break;
                    case "ffffff": /*white*/
                        settings.ButtonStyle = "c5-btn c7-borders c3-text c5-btn";
                        break;
                    case "482c78":/*purple*/
                        settings.ButtonStyle = "c6-btn c5-borders c5-text c6-btn";
                        break;
                    case "f89721":/*orange*/
                        settings.ButtonStyle = "c7-btn c5-borders c5-text c7-btn";
                        break;
                    case "00aeef":/*light blue*/
                        settings.ButtonStyle = "c8-btn c5-borders c5-text c8-btn";
                        break;
                    default:
                        settings.ButtonStyle = "c7-btn c5-borders c5-text c7-btn";
                        break;
                }
            }
            else
            {
                settings.ButtonStyle = "c1-btn c5-borders c5-text";
            }

           // return settings;



             

            if (!String.IsNullOrEmpty(panelColor))
            {
                switch (panelColor)
                {
                    case "004775":/*dark blue*/
                        settings.PanelColor = "nav-panel-1";
                        break;
                    case "666666":/*grey*/
                        settings.PanelColor = "nav-panel-2";
                        break;
                    case "1d1d1b":/*black*/
                        settings.PanelColor = "nav-panel-3";
                        break;
                    case "00cc99":/*light green*/
                        settings.PanelColor = "nav-panel-4";
                        break;
                    case "ffffff": /*white*/
                        settings.PanelColor = "nav-panel-5";
                        break;
                    case "482c78":/*purple*/
                        settings.PanelColor = "nav-panel-6";
                        break;
                    case "f89721":/*orange*/
                        settings.PanelColor = "nav-panel-7";
                        break;
                    case "00aeef":/*light blue*/
                        settings.PanelColor = "nav-panel-8";
                        break;
                    default:
                        settings.PanelColor = "nav-panel-2";
                        break;
                }
            }
            else
            {
                settings.PanelColor = "nav-panel-2 empty";
            }

            return settings;
        }


    }




        //public static string GetBackgroundImageStyle(IPublishedContent image = null, USNBackgroundOption backgroundImageOptions = null)
        //{
        //    string output = String.Empty;
        //    string backgroundImage = String.Empty;
        //    string backgroundStyle = String.Empty;
        //    string backgroundPosition = String.Empty;

        //    if (image != null)
        //    {
        //        backgroundImage = String.Format("background-image:url('{0}');", image.Url);

        //        if (backgroundImageOptions != null)
        //        {
        //            switch (backgroundImageOptions.Style)
        //            {
        //                case "Cover":
        //                    backgroundStyle = "background-repeat:no-repeat;background-size:cover;";
        //                    break;
        //                case "Full width":
        //                    backgroundStyle = "background-repeat:no-repeat;background-size:100% auto;";
        //                    break;
        //                case "Original":
        //                    backgroundStyle = "background-repeat:no-repeat;background-size:auto;";
        //                    break;
        //                case "Repeat":
        //                    backgroundStyle = "background-repeat:repeat;background-size:auto;";
        //                    break;
        //                case "Repeat X":
        //                    backgroundStyle = "background-repeat:repeat-x;background-size:auto;";
        //                    break;
        //                case "Repeat Y":
        //                    backgroundStyle = "background-repeat:repeat-y;background-size:auto;";
        //                    break;
        //                default:
        //                    backgroundStyle = "background-repeat:no-repeat;background-size:auto;";
        //                    break;
        //            }

        //            switch (backgroundImageOptions.Position)
        //            {
        //                case "Center / Top":
        //                    backgroundPosition = "background-position:center top;";
        //                    break;
        //                case "Center / Center":
        //                    backgroundPosition = "background-position:center center;";
        //                    break;
        //                case "Center / Bottom":
        //                    backgroundPosition = "background-position:center bottom;";
        //                    break;
        //                case "Right / Top":
        //                    backgroundPosition = "background-position:right top;";
        //                    break;
        //                case "Right / Center":
        //                    backgroundPosition = "background-position:right center;";
        //                    break;
        //                case "Right / Bottom":
        //                    backgroundPosition = "background-position:right bottom;";
        //                    break;
        //                case "Left / Top":
        //                    backgroundPosition = "background-position:left top;";
        //                    break;
        //                case "Left / Center":
        //                    backgroundPosition = "background-position:left center;";
        //                    break;
        //                case "Left / Bottom":
        //                    backgroundPosition = "background-position:left bottom;";
        //                    break;
        //                default:
        //                    backgroundPosition = "background-position:center center;";
        //                    break;

        //            }

        //        }

        //        output = backgroundImage + backgroundStyle + backgroundPosition;

        //        if (output != String.Empty)
        //        {
        //            output = String.Format(" style=\"{0}\"", output);
        //        }
        //    }

        //    return output;
        //}

    }
