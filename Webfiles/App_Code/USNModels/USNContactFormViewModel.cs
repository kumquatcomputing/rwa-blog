﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using System.Web.Mvc;

namespace USN.Models
{
    /// <summary>
    /// Summary description for ContactFormViewModel
    /// </summary>
    public class USNContactFormViewModel
    {

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }

        public string Telephone { get; set; }

        [Required]
        public string JobTitle { get; set; }

        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        //[BooleanRequired]
        [Display]
        public bool NewsletterSignup { get; set; }
        //[Display]
        //public bool ConsentPost { get; set; }
        //[Display]
        //public bool ConsentEmail { get; set; }
        //[Display]
        //public bool ConsentPhone { get; set; }

        public int CurrentNodeID { get; set; }

        public string PageType { get; set; }

        public string RecaptchaError { get; set; }
    }

    public class BooleanRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            if (value is bool)
                return (bool)value;
            else
                return true;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(
            ModelMetadata metadata,
            ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "booleanrequired"
            };
        }
    }
}