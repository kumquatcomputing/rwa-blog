﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Logging;
using System.Web.UI.WebControls;

namespace USN.BusinessLogic
{
    /// <summary>
    /// Summary description for USNMail
    /// </summary>
    public class USNMail
    {
        public static bool SendContactFormMail(ContactFormMailVariables mailVariables, out string lsErrorMessage)
        {
            lsErrorMessage = "";

            try
            {
                //Create MailDefinition 
                MailDefinition md = new MailDefinition();
                string lsSendTo = "";

                //specify the location of template 
                md.BodyFileName = "/usn/emailtemplates/nosignupcontactform.htm";
                md.IsBodyHtml = true;

                //Build replacement collection to replace fields in template 
                System.Collections.Specialized.ListDictionary replacements = new System.Collections.Specialized.ListDictionary();
                replacements.Add("<% formFirstName %>", mailVariables.FirstName);
                replacements.Add("<% formLastName %>", mailVariables.LastName);
                replacements.Add("<% formEmail %>", mailVariables.Email);
                replacements.Add("<% formPhone %>", mailVariables.Telephone);
                replacements.Add("<% formJobTitle %>", mailVariables.JobTitle);
                replacements.Add("<% formMessage %>", umbraco.library.ReplaceLineBreaks(mailVariables.Message));
                replacements.Add("<% formNewsletterSignup %>", mailVariables.NewsletterSignup == false ? "" : mailVariables.NewsletterSignup.ToString());
                //replacements.Add("<% ConsentPost %>", mailVariables.ConsentPost == false ? "" : mailVariables.ConsentPost.ToString());
                //replacements.Add("<% ConsentEmail %>", mailVariables.ConsentEmail == false ? "" : mailVariables.ConsentEmail.ToString());
                //replacements.Add("<% ConsentPhone %>", mailVariables.ConsentPhone == false ? "" : mailVariables.ConsentPhone.ToString());
                replacements.Add("<% WebsitePage %>", mailVariables.PageName);
                replacements.Add("<% WebsiteName %>", mailVariables.WebsiteName);

                //Adds checked status for email checkboxes where model value is true
                if (mailVariables.NewsletterSignup == true)
                {
                    mailVariables.NewsletterIsChecked = "checked";
                    md.BodyFileName = "/usn/emailtemplates/contactform.htm";
                }
                replacements.Add("<% NewsletterIsChecked %>", mailVariables.NewsletterIsChecked);
                //if (mailVariables.ConsentPost == true)
                //{
                //    mailVariables.PostIsChecked = "checked";
                //}
                //if (mailVariables.ConsentEmail == true)
                //{
                //    mailVariables.EmailIsChecked = "checked";
                //} 
                //if (mailVariables.ConsentPhone == true)
                //{
                //    mailVariables.PhoneIsChecked = "checked";
                //}               
                //replacements.Add("<% PostIsChecked %>", mailVariables.PostIsChecked);
                //replacements.Add("<% EmailIsChecked %>", mailVariables.EmailIsChecked);
                //replacements.Add("<% PhoneIsChecked %>", mailVariables.PhoneIsChecked);

                lsSendTo = mailVariables.To;

                //now create mail message using the mail definition object 
                System.Net.Mail.MailMessage msg = md.CreateMailMessage(lsSendTo, replacements, new System.Web.UI.Control());
                msg.ReplyTo = new System.Net.Mail.MailAddress(mailVariables.Email);
                msg.Subject = mailVariables.WebsiteName + " Website: " + mailVariables.PageName + " Page Enquiry";

                //this uses SmtpClient in 2.0 to send email, this can be configured in web.config file.
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Send(msg);

                return true;
            }
            catch (Exception ex)
            {
                lsErrorMessage = ex.Message;
                LogHelper.Error<USNMail>("Error creating or sending Email.", ex);
            }

            return false;
        }

        public class ContactFormMailVariables
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Telephone { get; set; }
            public string JobTitle { get; set; }
            public string Message { get; set; }
            public bool NewsletterSignup { get; set; }
            //public bool ConsentPost { get; set; }
            //public bool ConsentEmail { get; set; }
            //public bool ConsentPhone { get; set; }
            public string To { get; set; }
            public string WebsiteName { get; set; }
            public string PageName { get; set; }


            //Checked strings added to email template if values are true.
            public string NewsletterIsChecked = "";
            //public string PostIsChecked = "";
            //public string EmailIsChecked = "";
            //public string PhoneIsChecked = "";
        }


        public static bool SendEnquiryFormMail(EnquiryFormMailVariables mailVariables, out string lsErrorMessage)
        {
            lsErrorMessage = "";

            try
            {
                //Create MailDefinition 
                MailDefinition md = new MailDefinition();
                string lsSendTo = "";
                string emailTemplate = "/usn/emailtemplates/sellenquiryform.htm";
                if (mailVariables.PageName == "Register to BUY")
                {
                    emailTemplate = "/usn/emailtemplates/buyenquiryform.htm";
                }
                //specify the location of template                 
                md.BodyFileName = emailTemplate;
                md.IsBodyHtml = true;

                //Build replacement collection to replace fields in template 
                System.Collections.Specialized.ListDictionary replacements = new System.Collections.Specialized.ListDictionary();
                replacements.Add("<% formKeyContact %>", mailVariables.KeyContact);
                replacements.Add("<% formCompany %>", mailVariables.Company);
                replacements.Add("<% formFcaNo %>", mailVariables.FcaNo);
                replacements.Add("<% formEmail %>", mailVariables.Email);
                replacements.Add("<% formPhone %>", mailVariables.Telephone);
                replacements.Add("<% formAddress %>", umbraco.library.ReplaceLineBreaks(mailVariables.Address));
                replacements.Add("<% formCompanyNo %>", mailVariables.CompanyNo);
                replacements.Add("<% formTargetLines %>", mailVariables.TargetLines);
                replacements.Add("<% formTargetTO %>", mailVariables.TargetTO);
                if (mailVariables.PageName == "Register to BUY")
                {
                    replacements.Add("<% formTargetLocation %>", mailVariables.TargetLocation);
                }
                if (mailVariables.PageName == "Register to SELL")
                {
                    replacements.Add("<% formNoOfBranches %>", mailVariables.NoOfBranches);
                    replacements.Add("<% formNoOfStaff %>", mailVariables.NoOfStaff);
                }
                replacements.Add("<% formMessage %>", umbraco.library.ReplaceLineBreaks(mailVariables.Message));
                replacements.Add("<% formNewsletterSignup %>", mailVariables.NewsletterSignup == false ? "" : mailVariables.NewsletterSignup.ToString());
                replacements.Add("<% ConsentPost %>", mailVariables.ConsentPost == false ? "" : mailVariables.ConsentPost.ToString());
                replacements.Add("<% ConsentEmail %>", mailVariables.ConsentEmail == false ? "" : mailVariables.ConsentEmail.ToString());
                replacements.Add("<% ConsentPhone %>", mailVariables.ConsentPhone == false ? "" : mailVariables.ConsentPhone.ToString());
                replacements.Add("<% WebsitePage %>", mailVariables.PageName);
                replacements.Add("<% WebsiteName %>", mailVariables.WebsiteName);

                //Adds checked status for email checkboxes where model value is true
                if (mailVariables.NewsletterSignup == true)
                {
                    mailVariables.NewsletterIsChecked = "checked";
                }
                //if (mailVariables.ConsentPost == true)
                //{
                //    mailVariables.PostIsChecked = "checked";
                //}
                //if (mailVariables.ConsentEmail == true)
                //{
                //    mailVariables.EmailIsChecked = "checked";
                //}
                //if (mailVariables.ConsentPhone == true)
                //{
                //    mailVariables.PhoneIsChecked = "checked";
                //}
                replacements.Add("<% NewsletterIsChecked %>", mailVariables.NewsletterIsChecked);
                //replacements.Add("<% PostIsChecked %>", mailVariables.PostIsChecked);
                //replacements.Add("<% EmailIsChecked %>", mailVariables.EmailIsChecked);
                //replacements.Add("<% PhoneIsChecked %>", mailVariables.PhoneIsChecked);

                lsSendTo = mailVariables.To;

                //now create mail message using the mail definition object 
                System.Net.Mail.MailMessage msg = md.CreateMailMessage(lsSendTo, replacements, new System.Web.UI.Control());
                msg.ReplyTo = new System.Net.Mail.MailAddress(mailVariables.Email);
                msg.Subject = mailVariables.WebsiteName + " Website: " + mailVariables.PageName + " Page Enquiry";

                //this uses SmtpClient in 2.0 to send email, this can be configured in web.config file.
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Send(msg);

                return true;
            }
            catch (Exception ex)
            {
                lsErrorMessage = ex.Message;
                LogHelper.Error<USNMail>("Error creating or sending Email.", ex);
            }

            return false;
        }

        public class EnquiryFormMailVariables
        {
            public string KeyContact { get; set; }
            public string Company { get; set; }
            public string FcaNo { get; set; }
            public string Email { get; set; }
            public string Telephone { get; set; }
            public string Address { get; set; }
            public string CompanyNo { get; set; }
            public string TargetLines { get; set; }
            public string TargetTO { get; set; }
            public string TargetLocation { get; set; }
            public string NoOfBranches { get; set; }
            public string NoOfStaff { get; set; }
            public string Message { get; set; }
            public string To { get; set; }
            public string WebsiteName { get; set; }
            public string PageName { get; set; }
            public bool NewsletterSignup { get; set; }
            public bool ConsentPost { get; set; }
            public bool ConsentEmail { get; set; }
            public bool ConsentPhone { get; set; }
            public int CurrentNodeID { get; set; }
            public string PageType { get; set; }

            //Checked strings added to email template if values are true.
            public string NewsletterIsChecked = "";
            //public string PostIsChecked = "";
            //public string EmailIsChecked = "";
            //public string PhoneIsChecked = "";
        }
    }
}