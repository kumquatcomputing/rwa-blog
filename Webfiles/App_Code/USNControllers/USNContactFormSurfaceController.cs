﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using createsend_dotnet;
using USN.BusinessLogic;
using USN.Models;
using Umbraco.Web;


using USNStarterKit.USNBusinessLogic;
using USNStarterKit.USNModels;
using System.Web.UI.WebControls;
using MailChimp;
using MailChimp.Helper;
using MailChimp.Lists;

using Recaptcha.Web;
using Recaptcha.Web.Mvc;

namespace USN.Controllers
{
    /// <summary>
    /// Summary description for ContactFormSurfaceController
    /// </summary>
    public class USNContactFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        public ActionResult Index(int NodeID, string PageType)
        {
            var model = new USNContactFormViewModel();
            model.CurrentNodeID = NodeID;            
            model.PageType = PageType;

            return PartialView("USNForms/USN_ContactForm", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleContactSubmit(USNContactFormViewModel model)
        {

          


            System.Threading.Thread.Sleep(1000);

            string returnValue = "";



            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();
            if (string.IsNullOrEmpty(recaptchaHelper.Response))
            {
                //returnValue = String.Format("<div class=\"spc alert alert-success alert-dismissible fade in contact-alert\" role=\"alert\"><div class=\"info\"><span>{0}</span> <button class=\"btn c1-bg c5-text c1-btn\" onclick=\"reload()\">Reload</button></div></div>", "Please click the 'I am not a robot' checkbox.");                
                //return Content(returnValue);
                // ModelState.AddModelError("RecaptchaError", RecaptchaError);
                return CurrentUmbracoPage();
            }
            //This bit causing errors
            //else
            //{
            //    RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();
            //    if (recaptchaResult != RecaptchaVerificationResult.Success)
            //    {
                   
            //    }
            //}


            if (!ModelState.IsValid)
            {
                return JavaScript(String.Format("$(ContactError{0}).show();$(ContactError{0}).html('{1}');", model.CurrentNodeID, umbraco.library.GetDictionaryItem("USN Contact Form General Error")));
            }

            //Need to get NodeID from hidden field. CurrentPage does not work with Ajax.BeginForm
            var currentNode = Umbraco.TypedContent(model.CurrentNodeID);

            IPublishedContent homeNode = currentNode.AncestorOrSelf(1);
            var settingsFolder = Umbraco.TypedContent(homeNode.GetProperty("websiteConfigurationNode").Value);
            var globalSettings = settingsFolder.Children.Where(x => x.DocumentTypeAlias == "USNGlobalSettings").First();

            var mail = new USNMail.ContactFormMailVariables
            {
                FirstName = model.FirstName == null ? "" : model.FirstName,
                LastName = model.LastName == null ? "" : model.LastName,
                Email = model.Email == null ? "" : model.Email,
                Telephone = model.Telephone == null ? "" : model.Telephone,
                JobTitle = model.JobTitle == null ? "" : model.JobTitle,
                Message = model.Message == null ? "" : model.Message,
                NewsletterSignup = model.NewsletterSignup,
                //ConsentPost = model.ConsentPost,
                //ConsentEmail = model.ConsentEmail, 
                //ConsentPhone = model.ConsentPhone,
                To = currentNode.GetProperty("recipientEmailAddress").Value.ToString(),
                WebsiteName = globalSettings.GetProperty("websiteName").Value.ToString(),
                PageName = currentNode.Parent.Parent.Name
            };

            string errorMessage = "";

            if (!USNMail.SendContactFormMail(mail, out errorMessage))
            {
                return JavaScript(String.Format("$(ContactError{0}).show();$(ContactError{0}).html('<div class=\"info\"><p>{1}</p><p>{2}</p></div>');", model.CurrentNodeID, umbraco.library.GetDictionaryItem("USN Contact Form Mail Send Error"), errorMessage));
            }

            //This code was causing HTTP header errors when sending the mail
            //try
            //{
            //    if (model.NewsletterSignup && globalSettings.HasValue("campaignMonitorAPIKey") &&
            //        (globalSettings.HasValue("defaultCampaignMonitorSubscriberListID") || currentNode.HasValue("campaignMonitorSubscriberListID")))
            //    {
            //        string subsciberListID = "";

            //        if (currentNode.GetProperty("campaignMonitorSubscriberListID").Value.ToString() != String.Empty)
            //            subsciberListID = currentNode.GetProperty("campaignMonitorSubscriberListID").Value.ToString();
            //        else
            //            subsciberListID = globalSettings.GetProperty("defaultCampaignMonitorSubscriberListID").Value.ToString();

            //        AuthenticationDetails auth = new ApiKeyAuthenticationDetails(globalSettings.GetProperty("campaignMonitorAPIKey").Value.ToString());

            //        Subscriber loSubscriber = new Subscriber(auth, subsciberListID);

            //        List<SubscriberCustomField> customFields = new List<SubscriberCustomField>();

            //        string lsSubscriberID = loSubscriber.Add(model.Email, model.FirstName + " " + model.LastName, customFields, false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return JavaScript(String.Format("$(ContactError{0}).show();$(ContactError{0}).html('<div class=\"info\"><p>{1}</p><p>{2}</p></div>');", model.CurrentNodeID, umbraco.library.GetDictionaryItem("USN Contact Form Signup Error"), ex.Message));
            //}

            returnValue = String.Format("<div class=\"page_component alert alert-success alert-dismissible fade in contact-alert\" role=\"alert\"><div class=\"info\">{0}</div></div>", currentNode.GetProperty("submissionMessage").Value.ToString());

            return Content(returnValue);
        }
    }
}